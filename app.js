const express = require("express");
const vhost = require("vhost");

let app = express();

app.use(vhost("p4.despliegue.pablotagu.com", 
                require(`${__dirname}/../despliegue-p4/app`).app));
app.use(vhost("p3.despliegue.pablotagu.com", 
                require(`${__dirname}/../despliegue-p3/app`).app));

app.listen(8083);